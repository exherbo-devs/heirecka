# Copyright 2014 Nikolay Orliuk
# Distributed under the terms of the GNU General Public License v2

if ever is_scm ; then
    require github [ user=msgpack pn="msgpack-c" release=c-${PV} suffix=tar.gz branch=c_master ]
else
    require github [ user=msgpack pn="msgpack-c" release=c-${PV} suffix=tar.gz ]
fi
require cmake

export_exlib_phases src_compile src_install

SUMMARY="MessagePack implementation for C"
DESCRIPTION="
MessagePack is an efficient binary serialization format. It lets you exchange data among multiple
languages like JSON. But it's faster and smaller. Small integers are encoded into a single byte, and
typical short strings require only one extra byte in addition to the strings themselves.
"
HOMEPAGE="https://msgpack.org/"

LICENCES="Boost-1.0"
MYOPTIONS="doc"

DEPENDENCIES="
    build+run:
        sys-libs/zlib
        doc? ( app-doc/doxygen )
    test:
        dev-cpp/gtest
"

CMAKE_SRC_CONFIGURE_TESTS=( "-DMSGPACK_BUILD_TESTS:BOOL=ON -DMSGPACK_BUILD_TESTS:BOOL=OFF" )
CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=ON
    # They aren't installed anyway, would need dev-libs/cjson
    -DMSGPACK_BUILD_EXAMPLES:BOOL=OFF
    -DMSGPACK_GEN_COVERAGE:BOOL=OFF
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=( 'doc Doxygen' )

msgpack_src_compile() {
    cmake_src_compile

    option doc && ecmake_build --target doxygen
}

msgpack_src_install() {
    cmake_src_install

    if option doc ; then
        edo pushd "${WORK}"/doc_c
        dodoc -r html
        edo popd
    fi
}

