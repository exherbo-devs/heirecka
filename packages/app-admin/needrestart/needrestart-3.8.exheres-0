# Copyright 2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=liske tag=v${PV} ]

SUMMARY="Restart daemons after library updates"
DESCRIPTION="
needrestart checks which daemons need to be restarted after library upgrades.
It is inspired by checkrestart from the debian-goodies package."

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-devel/gettext
    build+run:
        dev-lang/perl:=
        dev-perl/libintl-perl   [[ note = [ Locale::TextDomain ] ]]
        dev-perl/Module-Find
        dev-perl/Proc-ProcessTable
        dev-perl/Sort-Naturally
        dev-perl/TermReadKey
    suggestion:
        sys-apps/iucode-tool [[ description = [
            Test for pending microcode updates of Intel CPUs ]
        ]]
"

src_prepare() {
    default

    edo sed \
        -e "/DESTDIR/s:/usr/lib/:/usr/$(exhost --target)/lib/:" \
        -e "/DESTDIR/s:/usr/sbin:/usr/$(exhost --target)/bin:" \
        -i Makefile
}

src_install() {
    default

    # Remove hooks for "foreign" package managers to find SysV init scripts
    edo rm "${IMAGE}"/etc/needrestart/hook.d/{10-dpkg,20-rpm,30-pacman}

    doman man/*.1

    keepdir /var/cache/${PN}
}

