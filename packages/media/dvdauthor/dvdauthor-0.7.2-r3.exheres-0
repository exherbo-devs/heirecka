# Copyright 2009 Jan Meier
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix="tar.gz" ] \
    autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="A simple set of tools to help you author a DVD"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: graphicsmagick imagemagick ) [[ number-selected = at-most-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-libs/fribidi
        dev-libs/libxml2:2.0[>=2.6.0]
        media-libs/fontconfig
        media-libs/freetype:2
        media-libs/libdvdread
        media-libs/libpng:=
        providers:graphicsmagick? ( media-gfx/GraphicsMagick )
        providers:imagemagick? ( media-gfx/ImageMagick[>=5.5.7] )
"

WORK=${WORKBASE}/${PN}

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/84d971def13b7e6317eae44369f49fd709b01030.patch
    "${FILES}"/17609a2785d4ae7654183affd25e357a606a260d.patch
    "${FILES}"/1f7426707a6cd90673a01930838cc43fdfea1593.patch
    "${FILES}"/1910f8c9ad281d03d80970d672c3b839a879d0d3.patch
    "${FILES}"/ba1f36271ce31f0ac92444aaf58cc5fd7493464f.patch
    "${FILES}"/a0d57bebb3a5d7259a0508a07a898cfe044155a1.patch
    "${FILES}"/d926718e9aad9919fbd922ca1e0da8bc07cb67d7.patch
    "${FILES}"/${PN}-Use-pkg-config-to-find-FreeType-thanks-to-Lars-Wendl.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'providers:graphicsmagick graphicsmagick'
    'providers:imagemagick imagemagick'
)

src_prepare() {
    has_version 'media-gfx/ImageMagick[>=7]' \
        && expatch "${FILES}"/${PN}-0.7.2-imagemagick7.patch

    autotools_src_prepare
}

src_configure() {
    default

    # Disable (GCC) nested routines:
    # 1. For GCC this requires executable stack.
    # 2. Clang does not support nested routines.
    edo sed -i \
        -e '/HAVE_NESTED_ROUTINES/d' \
        src/config.h
}
